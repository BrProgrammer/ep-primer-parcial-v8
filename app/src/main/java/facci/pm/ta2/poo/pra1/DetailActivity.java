package facci.pm.ta2.poo.pra1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    String euro = "\u20ac";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");
        String idProducto = getIntent().getStringExtra("object_id");
        DataQuery query = DataQuery.get("item");
        query.getInBackground(idProducto, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {
                    TextView textViewTitle = (TextView)findViewById(R.id.TextViewTitle);
                    textViewTitle.setText((String) object.get("name"));
                    TextView textViewPrice = (TextView)findViewById(R.id.TextViewPrice);
                    textViewPrice.setText((String) object.get("price") + " " + euro);
                    ImageView  thumbnail=  (ImageView)findViewById(R.id.thumbnail);
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    TextView textViewTipo = findViewById(R.id.TextViewTipo);
                    textViewTipo.setText((String) object.get("type"));
                } else {
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent;
        intent = new Intent(DetailActivity.this, ResultsActivity.class);
        startActivity(intent);
        finish();

    }
}
